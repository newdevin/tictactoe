﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ShoppingCart.Domain;
using ShoppingCart.Service;

namespace ShoppingCart.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ShoppingCartController : ControllerBase
    {
        private readonly IShoppingCartService shoppingCartService;

        public ShoppingCartController(IShoppingCartService shoppingCartService)
        {
            this.shoppingCartService = shoppingCartService;
        }
        
        [HttpGet]
        public IActionResult Get(int userId)
        {
            if (userId < 0)
                return BadRequest($"invalid {nameof(userId)}");

            var cart = shoppingCartService.Get(userId);
            return Ok(cart);
        }
    }
}