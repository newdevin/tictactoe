﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Red : IDisposable
    {

        StreamReader reader;
        public Red()
        {
            reader = new StreamReader(@"c:\Temp");
        }
        bool disposed = false;
        public void Dispose()
        {
            dispose(true);
            GC.SuppressFinalize(this);
        }


        protected virtual void dispose(bool disposing)
        {
            if (disposed)
                return;

            if(disposing)
            {
                reader?.Dispose();
                reader = null;
            }

            //free unmanaged resources
            disposed = true;
        }

        ~Red()
        {
            dispose(false);
        }
    }
}
