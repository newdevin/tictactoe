﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ConsoleApp1
{
    public interface IAudit
    {
        Action<string> Audit { get; }

    }


    public class Auditor : IAudit
    {
        public Action<string> Audit => Console.WriteLine;
    }

    class Program
    {

        public static void Main(string[] args)
        {

            IAudit audit = new Auditor();
            audit.Audit("Hello World");

        }



    }


}

