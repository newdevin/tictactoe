﻿using System;
using System.Collections.Generic;

namespace ConsoleApp1
{
    public static class MyExt
    {
        public static IEnumerable<T> NotFilter<T>(this IEnumerable<T> col, Func<T, bool> predicate)
        {
            foreach (T i in col)
                if (!predicate(i))
                    yield return i;
        }

        public static void ForEach<T>(this IEnumerable<T> col, Action<T> a)
        {
            foreach (T i in col)
                a.Invoke(i);
        }
    }
}
