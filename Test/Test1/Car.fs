﻿

module Car 

open System.Xml.Linq
open System.Xml.Linq

    type destination = 
        | Gas of string
        | Home of string
        | Office of string
        | Stadium of string

    let getDistance destination = 
        match destination with
            | Gas _  -> 10
            | Home _ -> 25
            | Stadium _ -> 25
            | Office _ -> 50

    let gasDestination = Gas "Gas"

    let calculateRemainingPetrol currentPetrol distance =
         currentPetrol - distance 
       
    let driveTo currentPetrol destination = 
        let distance = getDistance destination
        let remainingPetrol = calculateRemainingPetrol currentPetrol distance
        match destination with
            | Gas _ -> remainingPetrol + 50 
            | _ -> remainingPetrol
    
    let gasStation = Gas ("gas")
    let statduim = Stadium ("stadium")
    let office = Office ("office")
       
    let a = driveTo 100 office
    let b = driveTo a statduim
    let c = driveTo b gasStation


            
        
 

