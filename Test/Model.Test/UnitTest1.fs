namespace Tests

open NUnit.Framework
open Model.Math

[<TestFixture>]
type MathTest () =
   
    [<Test>]
    member this.Expect2Add2Equals4() =
        let result = add 2 2;
        Assert.AreEqual( 4, result);
