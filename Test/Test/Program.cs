﻿using System;

namespace Test
{
    class Program
    {
        static void Main(string[] args)
        {

            var result = Kata.XO("ooxx");
            Console.WriteLine(result);


             result = Kata.XO("zzx");
            Console.WriteLine(result);



            Console.WriteLine("Hello World!");
        }
    }


    public static class Kata
    {
        public static bool XO(string input)
        {
            var firstCharRepeatCount = 0;
            var secondCharRepeatCount = 0;
            var firstCharToFind = 'o';
            var sencondCharToFind = 'x';
            foreach(var c in input)
            {
                if(c==Char.ToUpper(firstCharToFind))
                {
                    firstCharRepeatCount++;
                }
                else if(c == Char.ToUpper(sencondCharToFind))
                {
                    secondCharRepeatCount++;
                }
            }

            return firstCharRepeatCount == secondCharRepeatCount;
        }
    }
}
