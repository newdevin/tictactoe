﻿CREATE TABLE [dbo].[BookAuthor]
(
	[Id] INT NOT NULL PRIMARY KEY identity(1,1),
	[BookId] int not null foreign key references dbo.Book(Id),
	[AuthorId] int not null foreign key references dbo.Author(Id)
)
