﻿CREATE TABLE [dbo].[Author]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY(1,1),
	[FirstName] nvarchar (64) not null,
	[MiddleName] nvarchar(64),
	[LastName] nvarchar (64) not null,
	[Email] nvarchar (64)
)
