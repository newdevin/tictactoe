﻿using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace TicTacToe.Test
{
    public class PlayerTest
    {
        [Fact]
        public void PlayerIsCreated()
        {
            var playerName = "Derek";
            var player = Player.Create(playerName);
            Assert.True((player.IsSome));

            var name = player.Match(
                p => p.Name,
                () => throw new Exception(),
                e => throw e
                );
            Assert.Equal(playerName, name);
        }

        [Fact]
        public void PlayIsNotCreated()
        {
            var player = Player.Create(null);
            Assert.True((player.IsFaulted));

        }
    }
}
