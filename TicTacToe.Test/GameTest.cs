﻿using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace TicTacToe.Test
{
    public class GameTest
    {
        [Fact]
        public void WhenGameIsCreatedFirstPlayerIsCurrentPlayer()
        {
            var playerOne = Player.Create("Derek");
            Assert.True(playerOne.IsSome);

            var firstPlayer = playerOne.Match(
                p => p,
                () => throw new Exception(),
                e => throw e
                );


            var playerTwo = Player.Create("Smith");
            Assert.True(playerTwo.IsSome);

            var secondPlayer = playerTwo.Match(
                p => p,
                () => throw new Exception(),
                e => throw e
                );

            var game = new Game(firstPlayer, secondPlayer);
            Assert.Same(firstPlayer, game.CurrentPlayer);
        }

        [Fact]
        public void WhenTheGameStartsTheBoardIsEmpty()
        {
            var game = GetNewGame("Derek", "Smith");
            Assert.True(game.Board.IsEmpty);
        }

        private static Game GetNewGame(string firstPlayerName, string secondPlayerName)
        {
            var playerOne = Player.Create(firstPlayerName);
            Assert.True(playerOne.IsSome);

            var firstPlayer = playerOne.Match(
                p => p,
                () => throw new Exception(),
                e => throw e
                );


            var playerTwo = Player.Create(secondPlayerName);
            Assert.True(playerTwo.IsSome);

            var secondPlayer = playerTwo.Match(
                p => p,
                () => throw new Exception(),
                e => throw e
                );

            var game = new Game(firstPlayer, secondPlayer);
            return game;
        }

        [Fact]
        public void WhenPlayerOneMakesAMoveThenTheCurrentPlayerIsPlayerTwo()
        {
            var game = GetNewGame("Derek", "Smith");
            game.Move(game.PlayerOne, 1);
            Assert.True(game.CurrentPlayer == game.PlayerTwo);
        }

        [Fact]
        public void WhenPlayerTwoMakesAMoveThenTheCurrentPlayerIsPlayerOne()
        {
            var game = GetNewGame("Derek", "Smith");
            game.Move(game.PlayerOne, 1);
            game.Move(game.PlayerTwo, 2);
            Assert.True(game.CurrentPlayer == game.PlayerOne);
        }

        [Fact]
        public void IfPlayerTwoIsNotCurrentPlayerAndPlayerTwoMovesThenExceptionIsThrown()
        {
            var game = GetNewGame("Derek", "Smith");
            Assert.Throws<InvalidOperationException>(() => game.Move(game.PlayerTwo, 1));
        }

        [Fact]
        public void IfPlayerOneIsNotCurrentPlayerAndPlayerOneMovesItIsAnError()
        {
            var game = GetNewGame("Derek", "Smith");
            game.Move(game.PlayerOne, 1);
            Assert.Throws<InvalidOperationException>(() => game.Move(game.PlayerOne, 1));

        }

        [Fact]
        public void IfPlayerMoveToInvalidLocationThenExceptionIsThrown()
        {
            var game = GetNewGame("Derek", "Smith");
            Assert.Throws<InvalidOperationException>(() => game.Move(game.PlayerOne, 10));
        }

        [Fact]
        public void WhenPlayerOneMovesTheBoardIsMarkedWithXAtThatLocation()
        {
            int position = 1;
            var game = GetNewGame("Derek", "Smith");
            game.Move(game.PlayerOne, position);
            Assert.Equal("X", game.Board[position]);
        }
        [Fact]
        public void WhenPlayerTwoMovesTheBoardIsMarkedWithOAtThatLocation()
        {
            int position = 1;
            var game = GetNewGame("Derek", "Smith");
            game.Move(game.PlayerOne, position);
            position++;
            game.Move(game.PlayerTwo, position);
            Assert.Equal("O", game.Board[position]);
        }

        [Fact]
        public void WhenALocationIsAlreadyTakenAndAPlayerMovedToThatLocationExceptionIsThrown()
        {
            int position = 1;
            var game = GetNewGame("Derek", "Smith");
            game.Move(game.PlayerOne, position);
            Assert.Throws<InvalidOperationException>(() => game.Move(game.PlayerTwo, position));

        }

        [Fact]
        public void WhenTheGameStartsThereIsNoWinner()
        {
            var game = GetNewGame("Derek", "Smith");
            Assert.Null(game.Winner);
        }

        [Fact]
        public void WhenAPlayerGetsAWinningCombinationItWins()
        {
            var game = GetNewGame("Derek", "Smith");
            game.Move(game.PlayerOne, 0);
            Assert.Null(game.Winner);
            game.Move(game.PlayerTwo, 1);
            Assert.Null(game.Winner);
            game.Move(game.PlayerOne, 3);
            Assert.Null(game.Winner);
            game.Move(game.PlayerTwo, 4);
            Assert.Null(game.Winner);
            game.Move(game.PlayerOne, 6);
            Assert.NotNull(game.Winner);
            Assert.Same(game.PlayerOne, game.Winner);

        }

        [Fact]
        public void WhenGameIsOverAnyMoreMovesThrowsException()
        {
            var game = GetNewGame("Derek", "Smith");
            game.Move(game.PlayerOne, 0);
            game.Move(game.PlayerTwo, 1);
            game.Move(game.PlayerOne, 3);
            game.Move(game.PlayerTwo, 4);
            game.Move(game.PlayerOne, 6);
            Assert.Same(game.PlayerOne, game.Winner);
            Assert.True(game.IsGameOver);

            Assert.Throws<InvalidOperationException>(() => game.Move(game.PlayerTwo, 7));
        }

        [Fact]
        public void WhenAPlayerWinsTheGameIsOver()
        {
            var game = GetNewGame("Derek", "Smith");
            game.Move(game.PlayerOne, 0);
            game.Move(game.PlayerTwo, 1);
            game.Move(game.PlayerOne, 3);
            game.Move(game.PlayerTwo, 4);
            game.Move(game.PlayerOne, 6);
            Assert.Same(game.PlayerOne, game.Winner);
            Assert.True(game.IsGameOver);

        }

        [Fact]
        public void WhenTheGameEndsInDrawThenThereAreNoWinner()
        {
            var game = GetNewGame("Derek", "Smith");
            game.Move(game.PlayerOne, 0);
            game.Move(game.PlayerTwo, 4);
            game.Move(game.PlayerOne, 3);
            game.Move(game.PlayerTwo, 6);
            game.Move(game.PlayerOne, 2);
            game.Move(game.PlayerTwo, 1);
            game.Move(game.PlayerOne, 7);
            game.Move(game.PlayerTwo, 5);
            game.Move(game.PlayerOne, 8);

            Assert.True(game.IsGameOver);
            Assert.Null(game.Winner);
        }


    }
}
