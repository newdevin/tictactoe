﻿using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace TicTacToe.Test
{
    public class BoardTest
    {

        [Fact]
        public void WithOnePositionIsWinnerIsFalse()
        {
            Board b = new Board();
            b.Set(0, "X");
            Assert.False(b.IsWinner("X"));

        }

        [Fact]
        public void ThreeCorrectWinningCombinationGiveIsWinnerTrue()
        {
            Board b = new Board();
            b.Set(0, "X");
            b.Set(1, "X");
            b.Set(2, "X");
            Assert.True(b.IsWinner("X"));
        }

        [Fact]
        public void ThreeOutOfFourCorrectWinningCombinationGiveIsWinnerTrue()
        {
            Board b = new Board();
            b.Set(0, "X");
            b.Set(1, "X");
            b.Set(3, "X");
            b.Set(2, "X");
            Assert.True(b.IsWinner("X"));
        }
    }
}
