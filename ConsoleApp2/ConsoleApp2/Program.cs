﻿using System;

namespace ConsoleApp2
{
    using LanguageExt;
    class Program
    {


        static void Main(string[] args)
        {
            

            Option<int> opt = GetANumber(7);
            opt = opt.Map((a) => 2 * a);

             opt.Match((i) =>
             {
                 Console.WriteLine(i);
             }, () =>
             {
                 Console.WriteLine("nothing");
             });
        }

        static Option<int> GetANumber(int i)
        {
            if (i % 2 == 0)
                return Some.Create(i);
            return OptionNone.Default;
        }

    }

  


}
