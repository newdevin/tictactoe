﻿using ShoppingCart.Domain;
using System.Collections.Generic;
using System.Text;

namespace ShoppingCart.Service
{
    public interface IShoppingCartService
    {
        Cart Get(int userId);
    }
}
