﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ShoppingCart.Domain
{
    public class Cart
    {
        private readonly int userId;
        private readonly Product product;

        private Cart(int userId, Product product)
        {
            this.userId = userId;
            this.product = product;
        }

        public static Cart Create(int userId, Product product)
        {
            return new Cart(userId, product);
        }
    }
}
