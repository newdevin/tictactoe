﻿using LanguageExt;
using static LanguageExt.Prelude;

namespace ShoppingCart.Domain
{
    public class Price
    {
        private readonly string currency;
        private readonly decimal amount;

        private Price(string currency, decimal amount)
        {
            this.currency = currency;
            this.amount = amount;
        }

        public static Either<PriceError, Price> Create(string currency, decimal amount)
        {

            var isValidCurrency = ValidateCurrency(currency);
            var isValidAmount = ValidateAmount(amount);

            return (isValidCurrency, isValidAmount)
                .Apply((curr, amt) => new Price(curr, amt))
                .Match<Either<PriceError, Price>>(p => p,
                e => new PriceError(string.Join(",", e)));
        }

        private static Validation<PriceError, string> ValidateCurrency(string currency)
        {
            if (string.IsNullOrWhiteSpace(currency))
                return Fail<PriceError, string>(new PriceError("invalid currency"));
            return Success<PriceError, string>(currency);
        }

        private static Validation<PriceError, decimal> ValidateAmount(decimal amount)
        {
            if (amount <= 0m)
                return Fail<PriceError, decimal>(new PriceError("amount should be grater than 0.0"));
            return Success<PriceError, decimal>(amount);
        }
    }
}