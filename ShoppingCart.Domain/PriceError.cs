﻿namespace ShoppingCart.Domain
{
    public class PriceError
    {
        private readonly string message;

        public PriceError(string message)
        {
            this.message = message;
        }
    }
}