﻿namespace ShoppingCart.Domain
{
    public class ProductError
    {
        private readonly string message;

        public ProductError(string message)
        {
            this.message = message;
        }
    }
}