﻿using LanguageExt;
using System;
using System.Collections.Generic;
using System.Text;
using static LanguageExt.Prelude;

namespace ShoppingCart.Domain
{
    public class Product
    {
        private readonly string name;
        private readonly string description;
        private readonly Price price;

        private Product(string name, string description, Price price)
        {
            this.name = name;
            this.description = description;
            this.price = price;
        }

        public static Either<ProductError, Product> Create(string name, string description, Price price)
        {
            var isValidName = IsValidName(name);
            var isValidDescription = IsValidDescription(description);

            return (isValidName, isValidDescription).Apply((n, d)
                => new Product(n, d, price))
                .Match<Either<ProductError, Product>>(prod => prod,
                error => new ProductError(string.Join(",", error)));

        }
        public static Validation<ProductError, string> IsValidName(string name)
        {
            if (string.IsNullOrWhiteSpace(name))
                return Fail<ProductError, string>(new ProductError($"Please provide a valid name for product"));
            return
                Success<ProductError, string>(name);
        }
        public static Validation<ProductError, string> IsValidDescription(string description)
        {
            if (string.IsNullOrWhiteSpace(description))
                return Fail<ProductError, string>(new ProductError($"Please provide a valid description for product"));
            return
                Success<ProductError, string>(description);
        }

    }
}
