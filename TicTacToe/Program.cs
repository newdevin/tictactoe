﻿using System;

namespace TicTacToe
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Welcome to Tic-Tac-Toe!");
            Console.WriteLine("Please enter player one name");
            var playerOneName = Console.ReadLine();

            Console.WriteLine("Please enter player two name");
            var playerTwoName = Console.ReadLine();

            var playerOne = GetPlayer(playerOneName);
            var playerTwo = GetPlayer(playerTwoName);

            var game = new Game(playerOne, playerTwo);

            while(!game.IsGameOver)
            {
                Console.WriteLine($"{game.CurrentPlayer.Name} move");
                var input = Console.ReadLine();
                int pos = -1;
                while (!int.TryParse(input, out pos))
                {
                    input = Console.ReadLine();
                    Console.WriteLine("Invalid input. Please enter a number between 0 and 8");
                }
                try
                {
                    game.Move(game.CurrentPlayer, pos);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                    
                }

                
            }

            Console.WriteLine("Game Over!!");
            if (game.Winner != null)
                Console.WriteLine($"The winner is {game.Winner.Name}");
            else
                Console.WriteLine("No winner");

        }

        static Player GetPlayer(string name)
        {
            var playerOne = Player.Create(name);
            
            return playerOne.Match(
                p => p,
                () => throw new Exception(),
                e => throw e
                );
        }
    }
}
