﻿using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace TicTacToe.Test
{
    public class PlayerTest
    {
        [Fact]
        public void PlayerIsCreated()
        {
            string playerName = "Derek";
            var player = Player.Create(playerName);

            Assert.True(player.IsSome);
            var actual = player.Match(p => p.Name,
                                      () => "",
                                      (e) => e.ToString());
            Assert.Equal(playerName, actual);
        }

        [Fact]
        public void PlayerIsNotCreated()
        {
            string playerName = "";
            var player = Player.Create(playerName);

            Assert.True(player.IsFaulted);
            var actual = player.Match(p => p.Name,
                                      () => "",
                                      (e) => e.ToString());
            Assert.NotEqual(playerName, actual);
        }

    }
}
