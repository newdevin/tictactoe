﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TicTacToe
{
    public class Game
    {
        public Board Board { get; }
        public Player CurrentPlayer { get; private set; }
        public Player Winner { get; private set; }
        public bool IsGameOver => Winner != null || Board.IsFull ;
        
        public Game(Player playerOne, Player playerTwo)
        {
            PlayerOne = playerOne;
            PlayerTwo = playerTwo;
            CurrentPlayer = playerOne;
            Board = new Board();
        }

        public Player PlayerOne { get; }
        public Player PlayerTwo { get; }

        public void Move(Player player, int position)
        {
            if (IsGameOver)
                throw new InvalidOperationException($"The game is over");

            if (CurrentPlayer != player)
                throw new InvalidOperationException($"Current player is {CurrentPlayer.Name}");

            if (position > 8)
                throw new InvalidOperationException($"{position} is an invalid position on board");

            if (Board.Taken(position))
                throw new InvalidOperationException($"{position} is already taken");

            if (CurrentPlayer == PlayerOne)
            {
                Board.Set(position, "X");
                if (Board.IsWinner("X"))
                    Winner = PlayerOne;
            }
            else
            {
                Board.Set(position, "O");
                if (Board.IsWinner("O"))
                    Winner = PlayerTwo;
            }

            CurrentPlayer = (player == PlayerTwo) ? PlayerOne : PlayerTwo;
        }
    }
}
