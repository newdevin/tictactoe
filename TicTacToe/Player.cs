﻿using LanguageExt;
using System;

namespace TicTacToe
{
    public class Player
    {
        private Player()
        {

        }

        public string Name { get; private set; }
        public static OptionalResult<Player> Create(string name)
        {
            if (string.IsNullOrEmpty(name))
                return new OptionalResult<Player>(new ArgumentException($"{nameof(name)} is not a valid value"));
            return new OptionalResult<Player>(new Player() { Name = name });
        }
    }
}