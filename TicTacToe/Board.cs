﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace TicTacToe
{
    public class Board
    {
        List<HashSet<int>> winningCombination = new List<HashSet<int>>()
        {
            new HashSet<int>(new int[]{ 0,1,2}),
            new HashSet<int>(new int[] {3,4,5}),
            new HashSet<int>(new int[] {6,7,8}),
            new HashSet<int>(new int[] {0,3,6}),
            new HashSet<int>(new int[] {1,4,7}),
            new HashSet<int>(new int[] {2,5,8}),
            new HashSet<int>(new int[] {0,4,8}),
            new HashSet<int>(new int[] {2,4,6})
        };


        Dictionary<int, string> state;
        public bool IsEmpty => state.Values.All(v => v == null);
        public bool IsFull => state.Values.All(v => v != null);
        public Board()
        {
            state = new Dictionary<int, string>()
            {
                { 0,null },{ 1,null },{ 2,null },{ 3,null },{ 4,null },{5,null },{ 6,null },{ 7,null },{ 8,null }
            };
        }

        public string this[int index] => state[index];

        public void Set(int position, string value)
        {
            state[position] = value;
        }

        internal bool Taken(int position)
        {
            return state[position] != null;
        }

        public bool IsWinner(string value)
        {
            var keys = new HashSet<int>(state.Where(k => k.Value == value).Select(kv => kv.Key));

            if (keys.Count() < 3)
                return false;

            foreach (var s in winningCombination)
            {
                if (keys.IsSupersetOf(s))
                    return true;
            }
            return false;
        }
    }
}