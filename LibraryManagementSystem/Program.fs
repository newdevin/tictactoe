﻿// Learn more about F# at http://fsharp.org

open System
open Email
open Library.Domain

[<EntryPoint>]
let main argv =
    let email = create "dsingh@yahoo.com"
    match email with
    | Ok e -> let author:Author = {Id =0; FirstName ="Devinder Singh"; LastName="Singh"; Email=e}
              let newAuthor = Database.addAuthor author 
              printfn "%A" newAuthor
    | Error e -> printfn "%s" e


    0 // return an integer exit code
