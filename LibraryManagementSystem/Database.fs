﻿module Database

open Library.Domain
open FSharp.Data.Sql
open FSharp.Data.Sql.Transactions

type sql = SqlDataProvider<Common.DatabaseProviderTypes.MSSQLSERVER, "Server=localhost; Database=Library; Trusted_Connection = true;">
let transactionOptions = {IsolationLevel = IsolationLevel.DontCreateTransaction; Timeout = System.TimeSpan.MaxValue}
let ctx = sql.GetDataContext(transactionOptions)

let addBook book = 
    let bookEntity = ctx.Dbo.Book.``Create(Language, Name)`` (book.Language.ToString(), book.Name)
    ctx.SubmitUpdates()
    {book with Id = bookEntity.Id}

let addAuthor (author:Author) = 
    let authorEntity = ctx.Dbo.Author.``Create(FirstName, LastName)``(author.FirstName, author.LastName)
    authorEntity.Email <- Email.value  (author.Email)
    ctx.SubmitUpdates()
    {author with Id = authorEntity.Id};





