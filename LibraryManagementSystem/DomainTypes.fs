﻿module Library.Domain 

open Email
open System

type Language = 
    |English
    |French
    |German
    |Spanish
    |Italian

type Author = {FirstName:string;LastName:string; Email:Email; Id:int}
type Book = {Name:string ; Authors : Author list; Language: Language; Id:int}


