﻿module Email
open System.Text.RegularExpressions
type Email = Email of string
let create email = 
    if Regex.IsMatch(email, "^\S+@\S+\.\S+$") then
        Ok (Email email)
    else
        Error "Invalid email"

let value (Email email) = email

