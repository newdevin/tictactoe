﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace EFCoreInAction
{
    class Program
    {
        static void Main(string[] args)
        {
            using (var context = new AppDbContext())
            {


                var review = context.Find<Review>(1);

                review.BookId = 2;

               // context.Add(book);
                context.SaveChanges();

                var books = context.Book.Include(b=>b.Reviews).ToList();
                foreach(var b in books)
                {
                    Console.WriteLine($"Book:{b.Name}, Author= {b.Author?.Name}");
                    foreach(var rev in b.Reviews)
                    {
                        Console.WriteLine($"Reviewer:{rev.ReviewerName}, Rating:{rev.Rating}");
                    }
                }
            }
        }
    }
    class Book
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public ICollection<Review> Reviews { get; set; } = new List<Review>();
        public Author Author { get; set; }
    }

    class Review
    {
        public int Id { get; set; }
        public string ReviewerName { get; set; }
        public int Rating { get; set; }
        public int BookId { get; set; }
        public Book Book { get; set; }
    }

    class Author
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int BookId { get; set; }
    }

    class AppDbContext : DbContext
    {
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer("server=localhost; database=EFCoreInAction;trusted_connection=true");
        }

        public DbSet<Book> Book { get; set; }
    }
}
